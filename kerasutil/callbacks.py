import keras
import tensorflow as tf
import kerasutil.metrics
from os import path

class TensorBoard(keras.callbacks.Callback):
    def __init__(
        self,
        model,
        log_dir='./logs',
        metrics = [
            kerasutil.metrics.loss('loss')
        ],
        subfolders = True):

        self.model = model
        self.log_dir = log_dir
        self.graph = tf.Graph()
        self.session = tf.Session(graph=self.graph)
        self.metrics = metrics
        self.subfolders = subfolders
        
        with self.graph.as_default():
            if self.subfolders:
                for metric in self.metrics:
                    tf.summary.scalar(metric.name, metric.get_tensor())
                    
                self.train_writer = tf.summary.FileWriter(path.join(self.log_dir, 'train'))
                self.validation_writer = tf.summary.FileWriter(path.join(self.log_dir, 'validation'))
            else:
                for metric in self.metrics:
                    tf.summary.scalar('train/' + metric.name, metric.get_train_tensor())
                    tf.summary.scalar('val/' + metric.name, metric.get_validation_tensor())

                self.writer = tf.summary.FileWriter(self.log_dir)
                
            self.merged = tf.summary.merge_all()

    def on_epoch_end(self, epoch, logs):
        if self.subfolders:
            fetches = {}

            for metric in self.metrics:
                tensor = metric.get_tensor()

                fetches[tensor] = metric.get_train_value(logs=logs, model=self.model)

            summary = self.session.run(self.merged, fetches)

            self.train_writer.add_summary(summary, epoch)
            self.train_writer.flush()

            fetches = {}

            for metric in self.metrics:
                fetches[metric.get_tensor()] = metric.get_validation_value(logs=logs, model=self.model)

            summary = self.session.run(self.merged, fetches)

            self.validation_writer.add_summary(summary, epoch)
            self.validation_writer.flush()
        else:
            fetches = {}

            for metric in self.metrics:
                fetches[metric.get_train_tensor()] = metric.get_train_value(logs=logs, model=self.model)
                fetches[metric.get_validation_tensor()] = metric.get_validation_value(logs=logs, model=self.model)

            summary = self.session.run(self.merged, fetches)

            self.writer.add_summary(summary, epoch)
            self.writer.flush()
