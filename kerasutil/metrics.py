import tensorflow as tf
import keras

class metric:
    def __init__(self, name):
        self.name = name
        self.tensor = None
        self.train_tensor = None
        self.validation_tensor = None

    def get_tensor(self):
        if self.tensor is None:
            self.tensor = tf.placeholder(name=self.name + '_placeholder', dtype=tf.float32, shape=())

        return self.tensor

    def get_train_tensor(self):
        if self.train_tensor is None:
            self.train_tensor = tf.placeholder(name='train_' + self.name + '_placeholder', dtype=tf.float32, shape=())

        return self.train_tensor

    def get_validation_tensor(self):
        if self.validation_tensor is None:
            self.validation_tensor = tf.placeholder(name='validation_' + self.name + '_placeholder', dtype=tf.float32, shape=())

        return self.validation_tensor

    def get_train_value(self, model, logs):
        pass

    def get_validation_value(self, model, logs):
        pass

class loss(metric):
    def __init__(self, name='loss'):
        super(loss, self).__init__(name)

    def get_train_value(self, logs, model=None):
        return logs['loss']

    def get_validation_value(self, logs, model=None):
        return logs['val_loss']

class accuracy(metric):
    def __init__(self, name='acc'):
        super(accuracy, self).__init__(name)

    def get_train_value(self, logs, model=None):
        return logs['acc']

    def get_validation_value(self, logs, model=None):
        return logs['val_acc']

class learning_rate(metric):
    def __init__(self, name='lr'):
        super(learning_rate, self).__init__(name)

    def get_train_value(self, model, logs=None):
        return keras.backend.eval(model.optimizer.lr)

    def get_validation_value(self, model, logs=None):
        return keras.backend.eval(model.optimizer.lr)